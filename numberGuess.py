
import random

gameLoop = True

while gameLoop:
    target = random.randint(1, 100)
    guess = 0
    guessCount = 0
    #print(target)
    while guess != target and guessCount < 5:
        guessCount += 1
        guess=int(input("Guess which number: "))

        if guess == target:
            print("You win!")
        elif guess > target:
            print("Your guess is too high!")
        else:
            print("Your guess is too low!")
    if guess == target:
        print("You guessed correctly in",guessCount,"guesses.")
    else:
        print("You lose! The correct number was",target,".")

    ask = input("Would you like to play again? Y/N ")
    if ask == "N" or ask == "n":
        gameLoop = False
        print("Thanks for playing!")

        

